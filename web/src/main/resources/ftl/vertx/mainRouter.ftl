package ${projectPackage}.verticle;

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.LoggerHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.ext.web.templ.TemplateEngine;
import ${projectPackage}.util.VertxUtil;

public class MainHttpRouter extends AbstractVerticle{

    private TemplateEngine engine;

    public void start() {
        HttpServer server = vertx.createHttpServer();

        Router router = Router.router(vertx);
        engine = VertxUtil.getTemplateEngine();

        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
        router.route("/resource/*").handler(StaticHandler.create("resource").setCachingEnabled(true));
        <#--router.route().handler(CorsHandler.create("vertx\\.io"));-->
        router.route().handler(LoggerHandler.create());

        router.route("/").handler(ctx->{
            engine.render(ctx, "/template/", "main.ftl", res->{
                ctx.response().putHeader("content-type", "text/html;charset=UTF-8");
                if (res.succeeded()) {
                    ctx.response().end(res.result());
                } else {
                    ctx.fail(res.cause());
                }
            });
        });

        VertxUtil.setRouter(router);

        server.requestHandler(router::accept).listen(8080);
    }
}
