package ldh.maker.component;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.code.BootstrapCreateCode;
import ldh.maker.code.CreateCode;
import ldh.maker.code.FreemarkerCreateCode;
import ldh.maker.db.SettingDb;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.sql.SQLException;

public class FreemarkerCodeUi extends CodeUi {

    public FreemarkerCodeUi(TreeItem<TreeNode> treeItem, String dbName, String tableName) {
        super(treeItem, dbName, tableName);
    }

    @Override
    protected ColumnUi createColumnPane(TreeItem<TreeNode> treeItem, String dbName, String tableName) {
        return new FreemarkerColumnUi(treeItem, dbName, tableName, this);
    }

    @Override
    protected CreateCode buildCreateCode(Table table) throws SQLException {
        SettingData data = SettingDb.loadData(treeItem.getValue().getParent(), dbName);
        CreateCode createCode = new FreemarkerCreateCode(data, treeItem, dbName, table);
        return createCode;
    }
}
