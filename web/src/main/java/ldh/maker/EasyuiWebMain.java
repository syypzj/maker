package ldh.maker;

import ldh.maker.component.EasyuiContentUiFactory;
import ldh.maker.util.UiUtil;

public class EasyuiWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new EasyuiContentUiFactory());
    }

    @Override
    protected String getTitle() {
        return "Easyui前后端--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}

