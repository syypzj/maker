package ldh.maker.vo;

/**
 * Created by ldh on 2017/4/23.
 */
public class EnumData {

    private Object value;
    private String desc;

    public EnumData(Object value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
