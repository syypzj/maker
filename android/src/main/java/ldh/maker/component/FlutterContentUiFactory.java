package ldh.maker.component;

/**
 * Created by ldh123 on 2018/9/10.
 */
public class FlutterContentUiFactory extends ContentUiFactory {

    public ContentUi create() {
        return new FlutterContentUi();
    }
}
